# Drinkgedrag.nl website

This is the website of Drinkgedrag.nl, a Dutch determinants study.
It is hosted here on [[Gitlab Pages](https://matherion.gitlab.io/drinkgedrag-website) and
its primary URL is https://drinkgedrag.nl (but not that during development, that URL might
be slightly outdated, as updates are not automatically pushed).
