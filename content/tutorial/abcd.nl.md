+++
title = "Acyclic Behavior Change Diagrams"

date = 2019-03-19T00:00:00
# lastmod = 2018-09-09T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

# Add menu entry to sidebar.
linktitle = "ABCD"
[menu.tutorial]
  parent = "Overzicht"
  weight = 2
+++

Om te leren over Acyclic Behavior Change Diagrams, zie:

- [Een algemene introductie](https://r-packages.gitlab.io/behaviorchange/articles/abcd-laagdrempelige_nederlandse_uitleg.html)
- [Een blog post die zich richt op praktische toepassing](https://sciencer.netlify.com/2019/02/een-voorbeeld-van-de-abcd-in-de-dagelijkse-praktijk-van-interventie-ontwikkeling/)
- [Een Engelstalige vignette](https://r-packages.gitlab.io/behaviorchange/articles/abcd.html)

