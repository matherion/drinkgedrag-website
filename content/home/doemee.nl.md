+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Doe mee"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  image = "headers/drinkgedrag-bg-cutout.jpg"  # Name of image in `static/img/`.
  image_darken = 0.2  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = true

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px; color:white;"
 
 # CSS class.
 css_class = ""
+++

De vragenlijst neemt ongeveer tussen de 15-30 minuten van uw tijd in beslag.

Meedoen aan dit onderzoek helpt op de volgende manieren:

- Het geeft richting aan preventieprogramma’s tegen overmatig drinken
- Het geeft aanwijzingen voor vervolgonderzoek
- Het helpt de GGD verder met hun valpreventie programma
- Het helpt hulpverlenende instanties om meer inzicht te krijgen in de onderliggende oorzaken van overmatig alcoholgebruik

De vragenlijst wordt afgenomen in de beveiligde omgeving van de Open Universiteit. Er worden geen persoonsgegevens opgeslagen. De data uit dit onderzoek worden, volgens de standaarden van de wetenschap, openbaar gemaakt. Die kunt u na afloop van de studie op deze website vinden.

{{% alert note %}}
<a href="https://lab.ou.nl/ls/953278">Klik hier om mee te doen!</a>
{{% /alert %}}
