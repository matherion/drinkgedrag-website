+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Achtergrond"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "white"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  # image = "headers/drinkgedrag-bg-cutout.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.2  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px;"
 
 # CSS class.
 css_class = ""
+++

Drinkgedrag verandert voortdurend.

Bijvoorbeeld door een drukke baan of gezin. Sommige mensen drinken minder als ze meer verantwoordelijkheden hebben. Anderen gaan juist meer drinken ter ontspanning.

Drinkgedrag kan ook veranderen over tijd. In veranderende levensfases gaan mensen soms ook meer of minder drinken.

Sommige mensen houden hun drinkgedrag goed in de hand, en anderen verliezen regelmatig de controle - en sommigen vinden controleverlies fijn, en anderen niet.

En soms is het drinkgedrag van mensen problematisch. Die mensen willen we helpen. We richten ons specifiek op vrouwen, omdat er naar het drinkgedrag van vrouwen nog te weinig onderzoek is gedaan. Deze website gaat over deze studie.

Dit onderzoek wordt uitgevoerd bij de [Open Universiteit](https://ou.nl) door Rachel Koning-Woud, student klinische psychologie aan de Open Universiteit, onder leiding van universitair docenten Gjalt-Jorn Peters en Arjan Bos en in samenwerking met GGD Gooi en Vechtstreek.

We hebben uitgebreide achtergrondinformatie over deze studie toegevoegd op het Open Science Framework. De wiki pagina met extra uitleg staat [hier](https://osf.io/upy2f/wiki/home/). De resultaten van het onderzoek zullen zowel daar als op deze website worden gepubliceerd.
